/*================================
MINI-CAPSTONE 1 

Correctly answer 3 items to pass.

1 of 5) Create a function called "combineArrs" that combines the following arrays in the console:
let days = ["lunes", "martes", "miyerkules", "huwebes", "biyernes", "sabado", "linggo"];

let milestones = ["nang tayo'y magkakilala", "nang tayo'y muling magkita", "nagtapat ka ng 'yong pag-ibig", "ay inibig din kita", "ay puno ng pagmamahalan", "tayo'y biglang nagkatampuhan", "giliw ako'y iyong iniwan"];

====================================*/


let days = ["lunes", "martes", "miyerkules", "huwebes", "biyernes", "sabado", "linggo"];

let milestones = ["nang tayo'y magkakilala", "nang tayo'y muling magkita", "nagtapat ka ng 'yong pag-ibig", "ay inibig din kita", "ay puno ng pagmamahalan", "tayo'y biglang nagkatampuhan", "giliw ako'y iyong iniwan"];


function combineArrs(days, milestones){

  for(let i=0; i <= days.length; i++){
     console.log(days[i] + ", " + milestones[i]);
  }
}

combineArrs(days, milestones);
