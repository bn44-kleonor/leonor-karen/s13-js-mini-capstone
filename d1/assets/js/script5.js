/*================================
MINI-CAPSTONE 1 

Correctly answer 3 items to pass.

5 of 5) Create a series of functions that validate values passed when a user registers. 
Please see instructions below:

Create four (4) functions:
a)  checkUsername
    - has 1 parameter
    - checks if the given username has at least 8 characters
    - returns a boolean value */

let username;
function checkUsername(username){
    if( username.length >= 8){
        //console.log("test username 8 true");
        return true;
    } else {
        //console.log("test username 8 false");
        return false;
    }
}
console.log("Check Username: " + checkUsername("Username"));



/*b)  checkEmail
    - has 1 parameter
    - checks if the given email includes "@" and ".com"
    - returns a boolean value */

//let email = "abc@def.com"
// console.log(email[0]);
// console.log(email[1]);
// console.log(email[2]);
// console.log(email[3]);
// console.log(email[4]);
// console.log(email[5]);
// console.log(email[15]);
// email.slice(1,-1)
//console.log('@' === '@');
//console.log('.' === '.');
//console.log(email.slice(email.length-4,email.length)) //.com


let email = "abc@y.com";
let valid = false;
function checkEmail(email) {
    let i = 0;
    while(i != email.length){
        //console.log("test-while");
        if(email[i] === '@'){
            //console.log(email[i]);
            if(email.slice(email.length-4,email.length) == '.com'){
                //console.log("test1");
                return true;
            } else {
                return false;
                //console.log("test2");
            }
        }
        i++;
    }
    return false;
}
console.log("Check Email: " + checkEmail("abc@y.com"));




/*c)  checkPasswords
    - has 2 parameters
    - checks if password length has at least 5 characters
    - checks if password is strictly equal to password2
    - returns a boolean value or a string */

//let pw = "pAssword";
function checkPasswords(pw, pwConf){
    if(pw.length >= 5) {
        //console.log("test 5 char");
        if(pw === pwConf) {
            //console.log("test true");
            return true;
        } else {
            //console.log("test false");
            //return false;
            return "Passwords should match";
        }
    } else {
        //console.log("test 5 char false");
        //return false;
        return "Passwords should be at least 5 characters";
    }
}

console.log("Check Password: " + checkPasswords("password", "password"));





/*d)  register
    - has four parameters: username, email, password, password2

    - checks values of other functions mentioned
    
    - if values meet conditions, returns "You are now registered. Welcome, ________!"

    - returns ALL appropriate errors when registration fails:
    Username should be at least 8 characters. You only gave only __.
    Email is not valid
    Passwords should be at least 5 characters
    Passwords should match

    - returns the ff message if one or more field is not provided:
    Please fill out all fields. */

function register(username, email, password, password2){

    if(checkUsername(username) == true){
        if(checkEmail(email) == true){
            if(checkPasswords(password, password2) == true){
                return `You are now registered. Welcome, ${username} !`
            } else {
                return checkPasswords(password, password2);
            }
        } else{
            return `Email is not valid.`;
        }
    } else {
        return `Username should be at least 8 characters. You only gave only ${username.length}.`;
    }


// checkUsername(username);
// checkEmail(email);
// checkPasswords(password, password2);
}

console.log("REGISTER below result 1");
//register(username, email, password, password2)
console.log(register("username", "y@a.com", "password", "password"));
console.log(" ");

console.log("REGISTER below result 2");
console.log(register("usernam", "y@a.com", "password", "password"));
console.log(" ");

console.log("REGISTER below result 3");
console.log(register("username", "ya.com", "password", "password"));
console.log(" ");

console.log("REGISTER below result 4");
console.log(register("username", "y@a.c", "password", "password"));
console.log(" ");

console.log("REGISTER below result 5");
console.log(register("username", "y@a.com", "pass", "password"));
console.log(" ");

console.log("REGISTER below result 6");
console.log(register("username", "y@a.com", "passWORD", "password"));
console.log(" ");


/*==============================*/
